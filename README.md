# NextLabs Assessment

# Section 1:

Task 1 : 
Bootstrap - https://sakthivel-selvam-mean-developer.github.io/NextLabs/
        
# Section 2:

Task 1 : 
Folder Structure - https://sakthivel-selvam-mean-developer.github.io/Folder-Structure/
    
Task 2 : 
Lazy Loading - https://sakthivel-selvam-mean-developer.github.io/Lazy-Loading/

# Section 3:
 1. Write and share a small note about your choice of system to schedule periodic tasks (such as downloading list of ISINs every 24 hours). Why did you choose it? Is it reliable enough; Or will it scale? If not, what are the problems with it? And, what else would you recommend to fix this problem at scale in production?

Solutions:
 
    For scheduling periodic tasks, such as downloading a list of ISINs every 24 hours, I would recommend using a task scheduler like cron in a Unix or Task Scheduler in a Windows environment.

    in cron:
        Reliability
        Simplicity
        Scalability

    in Task Scheduler:
        Native Integration
        Ease of Use
        Resource Efficiency

 2. Suppose you are building a financial planning tool - which requires us to fetch bank statements from the user. Now, we would like to encrypt the data - so that nobody - not even the developer can view it. What would you do to solve this problem?

Solutions:

    To ensure the security and privacy of user bank statements in a financial planning tool, you should implement end-to-end encryption.
    
    Secure Connection like TLS
    Client-Side Encryption
    Server-Side Encryption
    Secure Transmission
    Authentication and Authorization
